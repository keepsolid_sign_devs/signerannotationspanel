import QtQuick 2.0
import QtQuick.Controls 2.2

Rectangle {

    id : buttonRectangle

    signal clicked;

    property string normalImageSource;
    property string hoveredImageSource;
    property string clickedImageSource;
    property string disableImageSource;

    // public:
    property alias isHoverEnabled: mouseArea.hoverEnabled
    property bool isEnable: true

    Item {
        id: internalProperties
        property bool isHovered: false
      }


    states: [
        State {
            name: "NORMAL"
            when: (buttonRectangle.isEnable == true && internalProperties.isHovered == false)
            PropertyChanges { target: image; source: buttonRectangle.normalImageSource}
        },
        State {
            name: "CLICKED"
            PropertyChanges { target: image; source: buttonRectangle.clickedImageSource}
        },
        State {
            name: "HOVERED"
            when: (buttonRectangle.isEnable == true && internalProperties.isHovered == true)
            PropertyChanges { target: image; source: buttonRectangle.hoveredImageSource}
        },
        State {
            name: "DISABLED"
            when: (buttonRectangle.isEnable == false)
            PropertyChanges { target: image; source: buttonRectangle.disableImageSource}
        }
    ]

    width: 100
    height: 100

    Image {
        id: image
        anchors.fill: parent
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        onClicked: {

            if (buttonRectangle.isEnable == true)
            {
                buttonRectangle.clicked();
                buttonRectangle.state = "CLICKED"
                timer.running = true;
            }
        }

        hoverEnabled: true

        onEntered: {
            if (buttonRectangle.isEnable == true)
            {
                internalProperties.isHovered = true;
                buttonRectangle.state = "HOVERED"
            }
        }

        onExited: {
            if (buttonRectangle.isEnable == true)
            {
                internalProperties.isHovered = false;
                buttonRectangle.state = "NORMAL"
            }
        }
    }

    Timer {
            id: timer
            interval: 150
            repeat: false
            running: false
            triggeredOnStart: false
            onTriggered: {

                buttonRectangle.state = buttonRectangle.isEnable ? (internalProperties.isHovered ? "HOVERED" : "NORMAL" ) : "DISABLED"
            }
        }
}
