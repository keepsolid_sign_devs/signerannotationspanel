#include "annotationspanelcontroller.h"
#include <QTimer>

AnnotationsPanelController::AnnotationsPanelController(QObject *parent) : QAbstractItemModel(parent), m_annotationsList(), m_fillMode(FillMode_Author)
{

}

void AnnotationsPanelController::setupForFillMode(FillMode fillMode)
{
    m_fillMode = fillMode;
    this->fillWithTestDataForMode(m_fillMode);
}

void AnnotationsPanelController::fillWithTestDataForMode(FillMode fillmode)
{
    switch (fillmode)
    {
    case FillMode_Author:
    {
        AnnotationData ad_1 = AnnotationData(CSAnnotationType_Signature,
                                             0,
                                             true,
                                             1);

        AnnotationData ad_2 = AnnotationData(CSAnnotationType_Initials,
                                             1,
                                             false,
                                             2);

        AnnotationData ad_3 = AnnotationData(CSAnnotationType_Date,
                                             2,
                                             true,
                                             5);

        AnnotationData ad_4 = AnnotationData(CSAnnotationType_Checkmark,
                                             3,
                                             false,
                                             3);

        AnnotationData ad_5 = AnnotationData(CSAnnotationType_Text,
                                             4,
                                             false,
                                             3);

        addAnnotation(ad_1);
        addAnnotation(ad_2);
        addAnnotation(ad_3);
        addAnnotation(ad_4);
        addAnnotation(ad_5);
    }
        break;

    case FillMode_Signer:
    {
        AnnotationData ad_1 = AnnotationData(CSAnnotationType_Initials,
                                             0,
                                             true,
                                             1);

        AnnotationData ad_2 = AnnotationData(CSAnnotationType_Signature,
                                             1,
                                             false,
                                             2);

        AnnotationData ad_3 = AnnotationData(CSAnnotationType_Date,
                                             2,
                                             true,
                                             5);

        AnnotationData ad_4 = AnnotationData(CSAnnotationType_Checkmark,
                                             3,
                                             false,
                                             3);

        AnnotationData ad_5 = AnnotationData(CSAnnotationType_Text,
                                             4,
                                             false,
                                             3);

        AnnotationData ad_6 = AnnotationData(CSAnnotationType_Initials,
                                             5,
                                             true,
                                             1);

        AnnotationData ad_7 = AnnotationData(CSAnnotationType_Signature,
                                             6,
                                             false,
                                             2);

        AnnotationData ad_8 = AnnotationData(CSAnnotationType_Date,
                                             7,
                                             true,
                                             5);

        AnnotationData ad_9 = AnnotationData(CSAnnotationType_Checkmark,
                                             8,
                                             true,
                                             3);

        AnnotationData ad_10 = AnnotationData(CSAnnotationType_Text,
                                             9,
                                             true,
                                             3);

        addAnnotation(ad_1);
        addAnnotation(ad_2);
        addAnnotation(ad_3);
        addAnnotation(ad_4);
        addAnnotation(ad_5);
        addAnnotation(ad_6);
        addAnnotation(ad_7);
        addAnnotation(ad_8);
        addAnnotation(ad_9);
        addAnnotation(ad_10);

        QTimer::singleShot(2000, this, SLOT(testSelectSecondAnnotation()));
        QTimer::singleShot(4000, this, SLOT(testSignFirstAnnpotation()));
    }
        break;

    default:
        break;
    }
}

void AnnotationsPanelController::testSelectSecondAnnotation()
{
    selectAnnotationWithId(2);
}

void AnnotationsPanelController::testSignFirstAnnpotation()
{
    m_annotationsList[0].setIsPlaceHolder(false);
}

void AnnotationsPanelController::deleteAllAnnotations()
{
    if (!m_annotationsList.isEmpty())
    {
        beginRemoveRows(QModelIndex(), 0, rowCount());

//        qDeleteAll(m_annotationsList.begin(), m_notificationsList.end());
        m_annotationsList.clear();

        endRemoveRows();

        emit annotationsListDidChange(m_annotationsList);
    }
}

void AnnotationsPanelController::deleteAnnotationAtIndex(int index)
{
    if (!m_annotationsList.isEmpty() && rowCount() > index)
    {
        beginRemoveRows(QModelIndex(), index, index);
        m_annotationsList.removeAt(index);
        endRemoveRows();

        emit annotationsListDidChange(m_annotationsList);
    }
}

void AnnotationsPanelController::deleteAnnotationWithId(unsigned int id)
{
    int notificationIndex = this->indexOfAnnotationWithId(id);
    if (notificationIndex >= 0)
    {
        this->deleteAnnotationAtIndex(notificationIndex);
    }
}

int AnnotationsPanelController::indexOfAnnotationWithId(unsigned int id)
{
    int index = -1;

    for (int i = 0; i < m_annotationsList.size(); ++i) {
        if (m_annotationsList.at(i).getId() == id)
        {
            index = i;
            break;
        }
    }

    return index;
}

unsigned int AnnotationsPanelController::annotationIdAtIndex(int index)
{
    if (!m_annotationsList.isEmpty() && rowCount() > index)
    {
        AnnotationData ad = m_annotationsList[index];
        return ad.getId();
    }

    return -1;
}

unsigned int AnnotationsPanelController::annotationsCount()
{
    return m_annotationsList.count();
}

void AnnotationsPanelController::addAnnotation(AnnotationData annotationDataToAdd)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_annotationsList.append(annotationDataToAdd);
    endInsertRows();

    emit annotationsListDidChange(m_annotationsList);
}

unsigned int AnnotationsPanelController::getSelectedAnnotationId()
{
    return this->selectedAnnotationId;
}

void AnnotationsPanelController::setSelectedAnnotationId(unsigned int id)
{
    if (this->selectedAnnotationId != id)
    {
        this->selectedAnnotationId = id;

        int index = getSelectedAnnotationIndex();
        emit selectedAnnotationIndexChanged(index);

        if (m_fillMode == FillMode_Author)
        {
            qDebug(" --- qt --- Author mode");

            QString annotationType = nullptr;

            switch (index)
            {
            case CSAnnotationType_Signature:
                annotationType = "Signature";
                break;
            case CSAnnotationType_Initials:
                annotationType = "Initials";
                break;
            case CSAnnotationType_Date:
                annotationType = "Date";
                break;
            case CSAnnotationType_Checkmark:
                annotationType = "Checkmark";
                break;
            case CSAnnotationType_Text:
                annotationType = "Text";
                break;
            default:
                annotationType = "Anknown annotation type";
                break;
            }

            qDebug(" --- qt --- did select annotation with type: %s", annotationType.toLatin1().constData());
        }
        else
        {
            qDebug(" --- qt --- Signer mode");
            qDebug(" --- qt --- did select annotation at index: %d", index);
        }
    }
}

unsigned int AnnotationsPanelController::getSelectedAnnotationIndex()
{
    int index = -1;
    for (int i = 0; i < m_annotationsList.size(); ++i)
    {
        AnnotationData ad = m_annotationsList[i];
        if (ad.getId() == this->selectedAnnotationId)
        {
            index = i;
            break;
        }
    }

    return index;
}

void AnnotationsPanelController::selectAnnotationWithId(unsigned int id)
{
    this->setSelectedAnnotationId(id);
}

int AnnotationsPanelController::signedAnnotationsCount()
{
    int count = 0;

    for (int i = 0; i < m_annotationsList.size(); ++i)
    {
        AnnotationData ad = m_annotationsList[i];
        if (ad.getIsPlaceHolder() == false)
            count++;
    }

    return count;
}

void AnnotationsPanelController::closeButtonDidClick()
{
    qDebug(" --- qt --- closeButtonDidClick:");
}

bool AnnotationsPanelController::isAuthorFillMode()
{
    return m_fillMode == FillMode_Author;
}

bool AnnotationsPanelController::isSignerFillMode()
{
    return m_fillMode == FillMode_Signer;
}

void AnnotationsPanelController::setTotalDocumentParticipantsCount(unsigned int count)
{
    m_totalDocumentParticipantsCount = count;
}

int AnnotationsPanelController::getTotalDocumentParticipantsCount()
{
    return m_totalDocumentParticipantsCount;
}

void AnnotationsPanelController::setWithAssignedAnnotationsDocumentParticipantsCount(unsigned int count)
{
    m_withAssignedAnnotationsDocumentParticipantsCount = count;
}

int AnnotationsPanelController::getWithAssignedAnnotationsDocumentParticipantsCount()
{
    return m_withAssignedAnnotationsDocumentParticipantsCount;
}

// AbstractItemModel:

int AnnotationsPanelController::rowCount(const QModelIndex & parent) const
{
    if (parent.isValid()) {

        return 0;
    }
    return m_annotationsList.size();
}

QVariant AnnotationsPanelController::data(const QModelIndex & index, int role) const
{
    if (index.row() < 0 || index.row() >= m_annotationsList.size())
        return QVariant();

    AnnotationData annotationData = m_annotationsList[index.row()];
    switch (role) {
    case AnnotationTypeRole:
        return annotationData.getType();
        break;

    case AnnotationIdRole:
        return annotationData.getId();
        break;

    case AnnotationIsPlaceholderRole:
        return annotationData.getIsPlaceHolder();
        break;

    case AnnotationTypeStringRole:
        return annotationData.getTypeString();
        break;

    case AnnotationPageLocationStringRole:
        return annotationData.getPageLocationString();
        break;

    case AnnotationTypePreviewSourceRole:
        return annotationData.getAnnotationTypePreviewSource();
        break;
    }

    return QVariant();
}

QModelIndex AnnotationsPanelController::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return createIndex(row, column, static_cast<void *>(0));
}

QModelIndex AnnotationsPanelController::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}

int AnnotationsPanelController::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}

QHash<int, QByteArray> AnnotationsPanelController::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[AnnotationTypeRole] = "annotationType";
    roles[AnnotationIdRole] = "id";
    roles[AnnotationIsPlaceholderRole] = "isPlaceholder";
    roles[AnnotationTypeStringRole] = "typeString";
    roles[AnnotationPageLocationStringRole] = "pageLocationString";
    roles[AnnotationTypePreviewSourceRole] = "annotationTypePreviewSource";
    return roles;
}

