import QtQuick 2.6
import QtQuick.Window 2.2
import "./" as Controls

Window {
    visible: true
    width: 600
    height: 500
    minimumHeight: 400
    minimumWidth: 400
    maximumHeight: 800
//    maximumWidth: 254
    title: qsTr("Signer Annotations Panel")

    Rectangle {

        anchors.fill: parent
        color: "blue"
        id:rootID
        DropArea {
            id: dropArea
            anchors.fill: parent
            onDropped: {
//                listView.model.remove(listView.dragItemIndex);
//                listView.dragItemIndex = -1;

                console.log(" --- qml --- did drop annotation")
            }
        }
    }

    Controls.SignerAnnotationsPanel {
        width: 254
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top

        state: (annotationsPanelController.isAuthorFillMode()) ? "AUTHOR_FILL" : "SIGNER_FILL";

    }
}
