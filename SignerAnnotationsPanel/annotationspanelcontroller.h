#ifndef ANNOTATIONSPANELCONTROLLER_H
#define ANNOTATIONSPANELCONTROLLER_H

#include <QAbstractItemModel>
#include <QObject>
#include <QList>

#include "annotationdata.h"

enum FillMode {

        FillMode_Author = 1,
        FillMode_Signer
    };

class AnnotationsPanelController : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit AnnotationsPanelController(QObject *parent = nullptr);
    enum DocumentRoles {

            AnnotationTypeRole = Qt::UserRole + 1,
            AnnotationIdRole,
            AnnotationIsPlaceholderRole,
            AnnotationTypeStringRole,
            AnnotationPageLocationStringRole,
            AnnotationTypePreviewSourceRole,
        };

    void setupForFillMode(FillMode fillMode);
    void fillWithTestDataForMode(FillMode fillmode);

    Q_INVOKABLE void deleteAllAnnotations();
    Q_INVOKABLE void deleteAnnotationAtIndex(int index);
    Q_INVOKABLE void deleteAnnotationWithId(unsigned int id);
    Q_INVOKABLE int indexOfAnnotationWithId(unsigned int id);
    Q_INVOKABLE unsigned int annotationIdAtIndex(int index);
    Q_INVOKABLE unsigned int annotationsCount();
    void addAnnotation(AnnotationData annotationDataToAdd);

    Q_INVOKABLE unsigned int getSelectedAnnotationId();
    Q_INVOKABLE void setSelectedAnnotationId(unsigned int id);
    Q_INVOKABLE unsigned int getSelectedAnnotationIndex();
    Q_INVOKABLE void selectAnnotationWithId(unsigned int id);
    Q_INVOKABLE int signedAnnotationsCount();

    Q_INVOKABLE void setTotalDocumentParticipantsCount(unsigned int count);
    Q_INVOKABLE int getTotalDocumentParticipantsCount();
    Q_INVOKABLE void setWithAssignedAnnotationsDocumentParticipantsCount(unsigned int count);
    Q_INVOKABLE int getWithAssignedAnnotationsDocumentParticipantsCount();

    Q_INVOKABLE void closeButtonDidClick();

    Q_INVOKABLE bool isAuthorFillMode();
    Q_INVOKABLE bool isSignerFillMode();

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index,
                  int role = Qt::DisplayRole) const;
    QModelIndex index(int row,
                      int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

private:
    QList <AnnotationData> m_annotationsList;
    unsigned int selectedAnnotationId;
    FillMode m_fillMode;
    int m_totalDocumentParticipantsCount;
    int m_withAssignedAnnotationsDocumentParticipantsCount;

protected:

    QHash<int, QByteArray> roleNames() const;
signals:
    void annotationsListDidChange(QList <AnnotationData> annotationsList);
    void selectedAnnotationIndexChanged(int index);

private slots:
    void testSelectSecondAnnotation();
    void testSignFirstAnnpotation();
};

#endif // ANNOTATIONSPANELCONTROLLER_H
