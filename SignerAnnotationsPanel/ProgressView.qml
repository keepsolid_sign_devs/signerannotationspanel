import QtQuick 2.0
import QtQuick.Controls 2.2

Item {

    property int minimum: 0
    property int maximum: 100
    property int value: 30
    property bool withRoundedEdges : true

    property color borderColor: "#332b88d9"
    property int borderWidth: 1
    property color completeColor: "#2b88d9"
    property color incompleteColor: "#daebf8"

    Rectangle {
        id: outerRectangle

        clip: true
        anchors.fill: parent
        radius: withRoundedEdges ? 0.5 * outerRectangle.height : 0
        border.color: borderColor
        border.width: borderWidth
        color: incompleteColor
    }

    Rectangle {
        id: innerRectangle

        property int progressWidth:  ( ( outerRectangle.width * ( value- minimum ) ) / ( maximum - minimum ) - 2 * borderWidth )

        x: borderWidth
        y: borderWidth
        width: progressWidth
        height: outerRectangle.height - 2 * borderWidth
        radius: withRoundedEdges ? 0.5 * innerRectangle.height : 0
        color: completeColor

        Behavior on width {
            SmoothedAnimation {
                velocity: 1200
            }
        }
    }
}
