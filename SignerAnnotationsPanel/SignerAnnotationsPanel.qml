import QtQuick 2.6

Rectangle {

    signal closeClicked;

    property alias titleRectangle: titleRectangle

    id: mainRectangle

    width: 254
    height: 500
    color: "#f9f9f9"

    state: "AUTHOR_FILL"

    states: [
        State {
            name: "AUTHOR_FILL"
            AnchorChanges { target: progressViewRectangle; anchors.bottom: parent.bottom; }
            AnchorChanges { target: annotationsListView; anchors.bottom: parent.bottom; anchors.top: parent.top}

            PropertyChanges { target: progressViewRectangle; anchors.bottomMargin: 0 }
            PropertyChanges { target: annotationsListView; anchors.bottomMargin: 74; anchors.topMargin: 50}

            PropertyChanges { target: annotationsListView; delegate: listViewDelegate_author}

        },
        State {
            name: "SIGNER_FILL"
            AnchorChanges { target: progressViewRectangle; anchors.top: parent.top}
            AnchorChanges { target: annotationsListView; anchors.bottom: parent.bottom; anchors.top: parent.top}

            PropertyChanges { target: progressViewRectangle; anchors.topMargin: 50}
            PropertyChanges { target: annotationsListView; anchors.bottomMargin: 0; anchors.topMargin: 125}

            PropertyChanges { target: annotationsListView; delegate: listViewDelegate_signer}
        }
    ]

    Rectangle {
        id: titleRectangle
        height: 50
        color: "#f9f9f9"
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0

        Text {
            id: titleText
            width: 170
            height: 19
            color: "#444444"
            text: qsTr("Annotations")
            anchors.left: parent.left
            anchors.leftMargin: 24
            anchors.top: parent.top
            anchors.topMargin: 15
            font.capitalization: Font.Capitalize
            font.weight: Font.Light
            font.family: "OpenSans"
            font.pixelSize: 18
        }

        Image {
            id: image
            x: 230
            width: 8
            height: 8
            anchors.top: parent.top
            anchors.topMargin: 24
            anchors.right: parent.right
            anchors.rightMargin: 16
            source: "Resources/CloseButtonIcons/iconCloseSidebar@2x.png"
        }

        MouseArea {
            id: closeButtonMouseArea
            x: 228
            width: 12
            height: 12
            anchors.right: parent.right
            anchors.rightMargin: 14
            anchors.top: parent.top
            anchors.topMargin: 22

            onClicked: {

                mainRectangle.closeClicked();
                console.log(" --- qml --- closeClicked")
                annotationsPanelController.closeButtonDidClick();
            }
        }
    }


    AnnotationsFillProgressView {
        id: progressViewRectangle

        width: 254
        height: 74

        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        color: "#f9f9f9"
        state: mainRectangle.state

        totalAnnotationsCount: annotationsPanelController.annotationsCount()
        signedAnnotationsCount: annotationsPanelController.signedAnnotationsCount()
        selectedAnnotationIndex: ((annotationsListView.count > 0) ? annotationsListView.currentIndex : 0)
        totalDocumentParticipantsCount: annotationsPanelController.getTotalDocumentParticipantsCount();
        withAssignedAnnotationsDocumentParticipantsCount: annotationsPanelController.getWithAssignedAnnotationsDocumentParticipantsCount();

        onClickedPreviousButton: {

            var possibleIndex = annotationsListView.currentIndex - 1;

            if (possibleIndex >= 0 && possibleIndex < annotationsListView.count)
            {
                var annotationId = annotationsPanelController.annotationIdAtIndex(possibleIndex);
                annotationsPanelController.selectAnnotationWithId(annotationId);
            }
        }

        onClickedNextButton: {

            var possibleIndex = annotationsListView.currentIndex + 1;

            if (possibleIndex >= 0 && possibleIndex < annotationsListView.count)
            {
                var annotationId = annotationsPanelController.annotationIdAtIndex(possibleIndex);
                annotationsPanelController.selectAnnotationWithId(annotationId);
            }
        }
    }

    ListView {
        id: annotationsListView

        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        snapMode: ListView.SnapOneItem
        clip: true
        focus: true

        model: annotationsPanelController
        interactive: (mainRectangle.state == "SIGNER_FILL")

        Connections {
            target: annotationsPanelController

            onAnnotationsListDidChange: {

                console.log(" --- qml --- onAnnotationsListDidChange:");
            }

            onSelectedAnnotationIndexChanged: {

                console.log(" --- qml --- onSelectedAnnotationIndexChanged:", index);
                annotationsListView.currentIndex = index
                progressViewRectangle.selectedAnnotationIndex = index;
            }
        }
    }

    Component {
        id: listViewDelegate_signer
        Rectangle {

            id: listViewDelegateRectangle_signer
            property int minHeight: 50
            property bool isHovered: false
            property bool isSelected: (annotationsListView.currentIndex == index)

            width: annotationsListView.width
            height: (titleId.height + titleId.y + 10 > minHeight) ? titleId.height + titleId.y + 10: minHeight

            border.width: isSelected ? 1 : 0
            border.color: "#332980cc"
            color: isSelected ? "#daebf8" : (isHovered ? "#fbffffff" : "#f9f9f9")

            Image {
                id:imageId
                width: 30
                height: 30
                anchors.left: parent.left
                anchors.leftMargin: 27
                anchors.top: parent.top
                anchors.topMargin: 10
                source: annotationTypePreviewSource
                smooth: true
            }

            Image {
                id:isPlaceholderImage
                width: 12
                height: 12
                anchors.right: parent.right
                anchors.rightMargin: 16
                anchors.top: parent.top
                anchors.topMargin: 19
                source: (isPlaceholder == false) ? "/Resources/AnnotationsImages/iconAnnotationComplete@2x.png" : ""
                smooth: true
            }

            Text {
                id:titleId
                anchors.left: parent.left
                anchors.leftMargin: 83
                anchors.top: parent.top
                anchors.topMargin: 12
                anchors.right: parent.right
                anchors.rightMargin: 40

                text: typeString
                color: "#444444"
                font.pointSize:12
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                //                font: "OpenSans-Semibold"
            }

            Text {
                id:subtitleId
                anchors.left: parent.left
                anchors.leftMargin: 83
                anchors.top: parent.top
                anchors.topMargin: 29
                anchors.right: parent.right
                anchors.rightMargin: 40

                text: pageLocationString
                color: "#8d8d8d"
                font.pointSize:9
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }

            MouseArea {
                id: listViewDelegateRectangleMouseArea
                anchors.fill: parent
                hoverEnabled: true
                onEntered: listViewDelegateRectangle_signer.isHovered = true
                onExited: listViewDelegateRectangle_signer.isHovered = false

                onClicked: {

                    var annotationId = annotationsPanelController.annotationIdAtIndex(index);
                    annotationsPanelController.selectAnnotationWithId(annotationId);
                }
            }
        }
    }

    Component {
        id: listViewDelegate_author
        Rectangle {

            id: listViewDelegateRectangle_author

            property int minHeight: 50
            property bool isHovered: false
            property bool isSelected: (annotationsListView.currentIndex == index)

            width: annotationsListView.width
            height: (subtitleId.height + subtitleId.y + 10 > minHeight) ? subtitleId.height + subtitleId.y + 10: minHeight

            border.width: isSelected ? 1 : 0
            border.color: "#332980cc"
            color: isSelected ? "#daebf8" : (isHovered ? "#fbffffff" : "#f9f9f9")

            Image {
                id:imageId
                width: 30
                height: 30
                anchors.left: parent.left
                anchors.leftMargin: 27
                anchors.top: parent.top
                anchors.topMargin: 10
                source: annotationTypePreviewSource
                smooth: true
            }

            Image {
                id:isPlaceholderImage
                width: 12
                height: 12
                anchors.right: parent.right
                anchors.rightMargin: 16
                anchors.top: parent.top
                anchors.topMargin: 19
                source: (isPlaceholder == false) ? "/Resources/AnnotationsImages/iconAnnotationComplete@2x.png" : ""
                smooth: true

                visible: false
            }

            Text {
                id:titleId
                anchors.left: parent.left
                anchors.leftMargin: 83
                anchors.top: parent.top
                anchors.topMargin: 19
                anchors.right: parent.right
                anchors.rightMargin: 40

                text: typeString
                color: "#444444"
                font.pointSize:12
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                //                font: "OpenSans-Semibold"
            }

            Text {
                id:subtitleId
                anchors.left: parent.left
                anchors.leftMargin: 83
                anchors.top: parent.top
                anchors.topMargin: 29
                anchors.right: parent.right
                anchors.rightMargin: 40

                visible: false

                text: pageLocationString
                color: "#8d8d8d"
                font.pointSize:9
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }

//            //----------------------------

//            Rectangle {
//                 id: dragRect
//                 width: listViewDelegateRectangle_author.width
//                 height: listViewDelegateRectangle_author.height
//                 color: "#00000000"

//                 Image {
//                     id:dragImageId
//                     width: 30
//                     height: 30
//                     anchors.left: parent.left
//                     anchors.leftMargin: 27
//                     anchors.top: parent.top
//                     anchors.topMargin: 10
//                     source: annotationTypePreviewSource
//                     smooth: true
//                 }

//                 visible: falce
//            }

//            Rectangle {
//                 id: dragRect
//                 width: 30
//                 height: 30
//                 color: "#00000000"

//                 Image {
//                     id:dragImageId
//                     width: 30
//                     height: 30
//                     anchors.fill: parent
//                     source: annotationTypePreviewSource
//                     smooth: true

//                     visible: falce
//                 }

//                 visible: falce
//            }


//            MouseArea {
//                id: listViewDelegateRectangleMouseArea
//                anchors.fill: parent
//                hoverEnabled: true
////                onEntered: dragRect.isHovered = true
////                onExited: dragRect.isHovered = false

//                onClicked: {
//                    var annotationId = annotationsPanelController.annotationIdAtIndex(index);
//                    annotationsPanelController.selectAnnotationWithId(annotationId);
//                }

//                drag.target: dragRect

//                drag.onActiveChanged: {
//                    if (listViewDelegateRectangleMouseArea.drag.active) {
////                        annotationsListView.dragItemIndex = index;


//                        console.log(" --- !!!! --- ")

////                        dragRect.x = coords.mouseX - itemImage.width / 2;
////                        dragRect.y = coords.mouseY - itemImage.width / 2;
//                        dragRect.visible = true;
//                        dragImageId.visible = true;
//                    }
//                    dragRect.Drag.drop();
//                }
//            }

//            states: [
//                State {
//                    when: dragRect.Drag.active
//                    ParentChange {
//                        target: dragRect
//                        parent: rootID
//                    }

//                    AnchorChanges {
//                        target: dragRect
//                        anchors.horizontalCenter: undefined
//                        anchors.verticalCenter: undefined
//                    }
//                }
//            ]

//            Drag.active: listViewDelegateRectangleMouseArea.drag.active
//            Drag.hotSpot.x: dragRect.width / 2
//            Drag.hotSpot.y: dragRect.height / 2

            //----------------------------

            MouseArea {
                id: listViewDelegateRectangleMouseArea
                anchors.fill: parent
                hoverEnabled: true
                onEntered: listViewDelegateRectangle_author.isHovered = true
                onExited: listViewDelegateRectangle_author.isHovered = false

                onClicked: {

                    var annotationId = annotationsPanelController.annotationIdAtIndex(index);
                    annotationsPanelController.selectAnnotationWithId(annotationId);
                }

                drag.target: listViewDelegateRectangle_author

                drag.onActiveChanged: {
                    if (listViewDelegateRectangleMouseArea.drag.active) {
//                        annotationsListView.dragItemIndex = index;
                        console.log(" --- !!! --- drag did start")
                    }
                    listViewDelegateRectangle_author.Drag.drop();
                }
            }

            states: [
                State {
                    when: listViewDelegateRectangle_author.Drag.active
                    ParentChange {
                        target: listViewDelegateRectangle_author
                        parent: rootID
                    }

                    AnchorChanges {
                        target: listViewDelegateRectangle_author
                        anchors.horizontalCenter: undefined
                        anchors.verticalCenter: undefined
                    }
                }
            ]

            Drag.active: listViewDelegateRectangleMouseArea.drag.active
            Drag.hotSpot.x: listViewDelegateRectangle_author.width / 2
            Drag.hotSpot.y: listViewDelegateRectangle_author.height / 2

            //----------------------------
        }
    }
}
