import QtQuick 2.0

Rectangle {
    id: progressViewRectangle

    state: "AUTHOR_FILL"

    states: [
        State {
            name: "AUTHOR_FILL"

            PropertyChanges { target: toRightRectangle; visible: false}
            PropertyChanges { target: toLeftRectangle; visible: false}
            PropertyChanges { target: bottomText; horizontalAlignment: Text.AlignHCenter}

        },
        State {
            name: "SIGNER_FILL"

            PropertyChanges { target: toRightRectangle; visible: true}
            PropertyChanges { target: toLeftRectangle; visible: true}
            PropertyChanges { target: bottomText; horizontalAlignment:  Text.AlignRight}
        }
    ]

    property int totalAnnotationsCount: 5
    property int signedAnnotationsCount: 2

    property int totalDocumentParticipantsCount: 5
    property int withAssignedAnnotationsDocumentParticipantsCount: 2

    property int selectedAnnotationIndex: 0

    signal clickedPreviousButton
    signal clickedNextButton

    function topTitleText()
    {
        return (progressViewRectangle.state == "AUTHOR_FILL") ?
                    ("You must have more than " + totalDocumentParticipantsCount + " annotations") :
                    ("You must filling " +  totalAnnotationsCount + " annotations");
    }

    function bottomTitleText()
    {
        return (progressViewRectangle.state == "AUTHOR_FILL") ?
                    (withAssignedAnnotationsDocumentParticipantsCount + " from " + totalDocumentParticipantsCount) :
                    (signedAnnotationsCount + " from " + totalAnnotationsCount);
    }

    x: 0
    y: 0
    width: 254
    height: 74
    color: "#f9f9f9"

    Text {
        id: topText
        height: 12
        color: "#000000"
        text: topTitleText();
        anchors.leftMargin: 24
        font.family: "OpenSans"
        font.capitalization: Font.Capitalize
        anchors.rightMargin: 16
        anchors.topMargin: 4
        font.pixelSize: 10
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
    }

    ProgressView {
        id: progressRectangle
        height: 6
        anchors.leftMargin: 24
        anchors.rightMargin: 16
        anchors.topMargin: 26
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right

        borderColor: "#332b88d9"
        completeColor: "#2b88d9"
        incompleteColor: "#daebf8"
        borderWidth: 1

        minimum: 0
        maximum: 100
        value: (progressViewRectangle.state == "AUTHOR_FILL") ?
                   ((progressRectangle.maximum - progressRectangle.minimum) * withAssignedAnnotationsDocumentParticipantsCount / totalDocumentParticipantsCount) :
                   ((progressRectangle.maximum - progressRectangle.minimum) * signedAnnotationsCount / totalAnnotationsCount);
    }

    ButtonWithImage {
        id: toRightRectangle
        x: 224
        width: 14
        height: 14
        disableImageSource: "/Resources/RightButtonIcons/iconAnnotationNextDisable@2x.png"
        hoveredImageSource: "/Resources/RightButtonIcons/iconAnnotationNextHover@2x.png"
        normalImageSource: "/Resources/RightButtonIcons/iconAnnotationNextNormal@2x.png"
        clickedImageSource: "/Resources/RightButtonIcons/iconAnnotationNextClicked@2x.png"
        isHoverEnabled: true
        isEnable: (selectedAnnotationIndex != totalAnnotationsCount - 1)

        anchors.rightMargin: 16
        anchors.topMargin: 40
        anchors.top: parent.top
        anchors.right: parent.right

        onClicked: {
            clickedNextButton();
        }
    }

    ButtonWithImage {
        id: toLeftRectangle
        x: 205
        width: 14
        height: 14
        color: "#f9f9f9"
        disableImageSource: "/Resources/LeftButtonIcons/iconAnnotationPrevDisable@2x.png"
        hoveredImageSource: "/Resources/LeftButtonIcons/iconAnnotationPrevHover@2x.png"
        normalImageSource: "/Resources/LeftButtonIcons/iconAnnotationPrevNormal@2x.png"
        clickedImageSource: "/Resources/LeftButtonIcons/iconAnnotationPrevClicked@2x.png"
        isHoverEnabled: true
        isEnable: (selectedAnnotationIndex != 0)

        anchors.rightMargin: 35
        anchors.topMargin: 40
        anchors.top: parent.top
        anchors.right: parent.right

        onClicked: {
            clickedPreviousButton();
        }
    }

    Text {
        id: bottomText
        height: 12
        color: "#2779c1"
        text: bottomTitleText()
        anchors.left: parent.left
        anchors.leftMargin: 60
        anchors.right: parent.right
        anchors.rightMargin: 60
        font.family: "OpenSans"
        anchors.topMargin: 41
        font.pixelSize: 10
        anchors.top: parent.top
        horizontalAlignment: Text.AlignRight
    }
}
