#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "annotationspanelcontroller.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    AnnotationsPanelController *ac = new AnnotationsPanelController();
    ac->setupForFillMode(FillMode_Author);
//    ac->setupForFillMode(FillMode_Signer);

    ac->setTotalDocumentParticipantsCount(10);
    ac->setWithAssignedAnnotationsDocumentParticipantsCount(2);

    engine.rootContext()->setContextProperty("annotationsPanelController", ac);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
