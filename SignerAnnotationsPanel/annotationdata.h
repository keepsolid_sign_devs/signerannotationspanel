#ifndef ANNOTATIONDATA_H
#define ANNOTATIONDATA_H

#include <QObject>

enum CSAnnotationType {
    CSAnnotationType_Signature = 0,
    CSAnnotationType_Initials = 1,
    CSAnnotationType_Date = 2,
    CSAnnotationType_Checkmark = 3,
    CSAnnotationType_Text = 4
};

class AnnotationData
{
public:
    AnnotationData();
    AnnotationData(CSAnnotationType _type = CSAnnotationType_Signature,
                   unsigned int _id = 0,
                   bool _isPlaceHolder = true,
                   int _locationPageNumber = 0);

private:

    CSAnnotationType type;
    unsigned int id;
    bool isPlaceHolder;
    int locationPageNumber;

public:
    void setType(CSAnnotationType _type);
    CSAnnotationType getType();

    void setId(unsigned int _id);
    unsigned int getId() const;

    void setIsPlaceHolder(bool _isPlaceHolder);
    bool getIsPlaceHolder();

    void setLocationPageNumber(int _locationPageNumber);
    bool getLocationPageNumber();

    QString getTypeString() const;
    QString getPageLocationString() const;
    QString getAnnotationTypePreviewSource() const;
};

#endif // ANNOTATIONDATA_H
