#include "annotationdata.h"

AnnotationData::AnnotationData()
{

}

AnnotationData::AnnotationData(CSAnnotationType _type,
                               unsigned int _id,
                               bool _isPlaceHolder,
                               int _locationPageNumber) :
    type(_type),
    id(_id),
    isPlaceHolder(_isPlaceHolder),
    locationPageNumber(_locationPageNumber)
{

}

void AnnotationData::setType(CSAnnotationType _type)
{
    this->type = _type;
}

CSAnnotationType AnnotationData::getType()
{
    return this->type;
}

void AnnotationData::setId(unsigned int _id)
{
    this->id = _id;
}

unsigned int AnnotationData::getId() const
{
    return this->id;
}

void AnnotationData::setIsPlaceHolder(bool _isPlaceHolder)
{
    this->isPlaceHolder = _isPlaceHolder;
}

bool AnnotationData::getIsPlaceHolder()
{
    return this->isPlaceHolder;
}

void AnnotationData::setLocationPageNumber(int _locationPageNumber)
{
    this->locationPageNumber = _locationPageNumber;
}

bool AnnotationData::getLocationPageNumber()
{
    return this->locationPageNumber;
}

QString AnnotationData::getTypeString() const
{
    QString _typeString;

    switch (this->type) {
    case CSAnnotationType_Signature:
        _typeString = QString("Signature");
        break;
    case CSAnnotationType_Initials:
        _typeString = QString("Initials");
        break;
    case CSAnnotationType_Date:
        _typeString = QString("Date");
        break;
    case CSAnnotationType_Checkmark:
        _typeString = QString("Checkmark");
        break;
    case CSAnnotationType_Text:
        _typeString = QString("Text");
        break;
    default:
        _typeString = nullptr;
        break;
    }

    return _typeString;
}

QString AnnotationData::getPageLocationString() const
{
    return "Page " + QString::number(this->locationPageNumber);
}

QString AnnotationData::getAnnotationTypePreviewSource() const
{
    QString _previewSource;

    switch (this->type) {
    case CSAnnotationType_Signature:
        _previewSource = QString("/Resources/AnnotationsImages/iconAnnotationSignature@2x.png");
        break;
    case CSAnnotationType_Initials:
        _previewSource = QString("/Resources/AnnotationsImages/iconAnnotationInitials@2x.png");
        break;
    case CSAnnotationType_Date:
        _previewSource = QString("/Resources/AnnotationsImages/iconAnnotationDate@2x.png");
        break;
    case CSAnnotationType_Checkmark:
        _previewSource = QString("/Resources/AnnotationsImages/iconAnnotationCheckmark@2x.png");
        break;
    case CSAnnotationType_Text:
        _previewSource = QString("/Resources/AnnotationsImages/iconAnnotationText@2x.png");
        break;
    default:
        _previewSource = nullptr;
        break;
    }
    return _previewSource;
}
