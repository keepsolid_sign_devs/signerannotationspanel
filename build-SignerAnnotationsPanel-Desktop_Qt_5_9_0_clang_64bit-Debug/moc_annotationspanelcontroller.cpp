/****************************************************************************
** Meta object code from reading C++ file 'annotationspanelcontroller.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../SignerAnnotationsPanel/annotationspanelcontroller.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'annotationspanelcontroller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AnnotationsPanelController_t {
    QByteArrayData data[22];
    char stringdata0[453];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AnnotationsPanelController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AnnotationsPanelController_t qt_meta_stringdata_AnnotationsPanelController = {
    {
QT_MOC_LITERAL(0, 0, 26), // "AnnotationsPanelController"
QT_MOC_LITERAL(1, 27, 24), // "annotationsListDidChange"
QT_MOC_LITERAL(2, 52, 0), // ""
QT_MOC_LITERAL(3, 53, 21), // "QList<AnnotationData>"
QT_MOC_LITERAL(4, 75, 15), // "annotationsList"
QT_MOC_LITERAL(5, 91, 30), // "selectedAnnotationIndexChanged"
QT_MOC_LITERAL(6, 122, 5), // "index"
QT_MOC_LITERAL(7, 128, 26), // "testSelectSecondAnnotation"
QT_MOC_LITERAL(8, 155, 24), // "testSignFirstAnnpotation"
QT_MOC_LITERAL(9, 180, 20), // "deleteAllAnnotations"
QT_MOC_LITERAL(10, 201, 23), // "deleteAnnotationAtIndex"
QT_MOC_LITERAL(11, 225, 22), // "deleteAnnotationWithId"
QT_MOC_LITERAL(12, 248, 2), // "id"
QT_MOC_LITERAL(13, 251, 23), // "indexOfAnnotationWithId"
QT_MOC_LITERAL(14, 275, 19), // "annotationIdAtIndex"
QT_MOC_LITERAL(15, 295, 16), // "annotationsCount"
QT_MOC_LITERAL(16, 312, 23), // "getSelectedAnnotationId"
QT_MOC_LITERAL(17, 336, 23), // "setSelectedAnnotationId"
QT_MOC_LITERAL(18, 360, 26), // "getSelectedAnnotationIndex"
QT_MOC_LITERAL(19, 387, 22), // "selectAnnotationWithId"
QT_MOC_LITERAL(20, 410, 22), // "signedAnnotationsCount"
QT_MOC_LITERAL(21, 433, 19) // "closeButtonDidClick"

    },
    "AnnotationsPanelController\0"
    "annotationsListDidChange\0\0"
    "QList<AnnotationData>\0annotationsList\0"
    "selectedAnnotationIndexChanged\0index\0"
    "testSelectSecondAnnotation\0"
    "testSignFirstAnnpotation\0deleteAllAnnotations\0"
    "deleteAnnotationAtIndex\0deleteAnnotationWithId\0"
    "id\0indexOfAnnotationWithId\0"
    "annotationIdAtIndex\0annotationsCount\0"
    "getSelectedAnnotationId\0setSelectedAnnotationId\0"
    "getSelectedAnnotationIndex\0"
    "selectAnnotationWithId\0signedAnnotationsCount\0"
    "closeButtonDidClick"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AnnotationsPanelController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x06 /* Public */,
       5,    1,   97,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    0,  100,    2, 0x08 /* Private */,
       8,    0,  101,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
       9,    0,  102,    2, 0x02 /* Public */,
      10,    1,  103,    2, 0x02 /* Public */,
      11,    1,  106,    2, 0x02 /* Public */,
      13,    1,  109,    2, 0x02 /* Public */,
      14,    1,  112,    2, 0x02 /* Public */,
      15,    0,  115,    2, 0x02 /* Public */,
      16,    0,  116,    2, 0x02 /* Public */,
      17,    1,  117,    2, 0x02 /* Public */,
      18,    0,  120,    2, 0x02 /* Public */,
      19,    1,  121,    2, 0x02 /* Public */,
      20,    0,  124,    2, 0x02 /* Public */,
      21,    0,  125,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Int,    6,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::UInt,   12,
    QMetaType::Int, QMetaType::UInt,   12,
    QMetaType::UInt, QMetaType::Int,    6,
    QMetaType::UInt,
    QMetaType::UInt,
    QMetaType::Void, QMetaType::UInt,   12,
    QMetaType::UInt,
    QMetaType::Void, QMetaType::UInt,   12,
    QMetaType::Int,
    QMetaType::Void,

       0        // eod
};

void AnnotationsPanelController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AnnotationsPanelController *_t = static_cast<AnnotationsPanelController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->annotationsListDidChange((*reinterpret_cast< QList<AnnotationData>(*)>(_a[1]))); break;
        case 1: _t->selectedAnnotationIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->testSelectSecondAnnotation(); break;
        case 3: _t->testSignFirstAnnpotation(); break;
        case 4: _t->deleteAllAnnotations(); break;
        case 5: _t->deleteAnnotationAtIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->deleteAnnotationWithId((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 7: { int _r = _t->indexOfAnnotationWithId((*reinterpret_cast< uint(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 8: { uint _r = _t->annotationIdAtIndex((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< uint*>(_a[0]) = std::move(_r); }  break;
        case 9: { uint _r = _t->annotationsCount();
            if (_a[0]) *reinterpret_cast< uint*>(_a[0]) = std::move(_r); }  break;
        case 10: { uint _r = _t->getSelectedAnnotationId();
            if (_a[0]) *reinterpret_cast< uint*>(_a[0]) = std::move(_r); }  break;
        case 11: _t->setSelectedAnnotationId((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 12: { uint _r = _t->getSelectedAnnotationIndex();
            if (_a[0]) *reinterpret_cast< uint*>(_a[0]) = std::move(_r); }  break;
        case 13: _t->selectAnnotationWithId((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 14: { int _r = _t->signedAnnotationsCount();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 15: _t->closeButtonDidClick(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (AnnotationsPanelController::*_t)(QList<AnnotationData> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&AnnotationsPanelController::annotationsListDidChange)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (AnnotationsPanelController::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&AnnotationsPanelController::selectedAnnotationIndexChanged)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject AnnotationsPanelController::staticMetaObject = {
    { &QAbstractItemModel::staticMetaObject, qt_meta_stringdata_AnnotationsPanelController.data,
      qt_meta_data_AnnotationsPanelController,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *AnnotationsPanelController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AnnotationsPanelController::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AnnotationsPanelController.stringdata0))
        return static_cast<void*>(const_cast< AnnotationsPanelController*>(this));
    return QAbstractItemModel::qt_metacast(_clname);
}

int AnnotationsPanelController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractItemModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void AnnotationsPanelController::annotationsListDidChange(QList<AnnotationData> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void AnnotationsPanelController::selectedAnnotationIndexChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
